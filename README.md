# Input automation
## setup
This tool is based on the `xdotool` program, so even though opam should tell you to install it, make sure to have it working on your system.
## Overview
This is a libary defining a module `Script`, that allows you to create automation scripts in ocaml.
It supports mouse movement, clicking, and typing.
## How it works
Under the hood, it starts `xdotool`, but redirects its standard input so it is able to print commands directly into it.
