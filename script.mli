type command
type script
module Mouse: sig
  type button = Left | Right
  val click: ?button:button -> unit -> command
  val press: ?button:button -> unit -> command
  val release: ?button:button -> unit -> command
  val move: int -> int -> command
  val drag:
    ?button:button ->
    initial:(int * int) ->
    ?checkpoints:((int * int) list) ->
    final:(int * int) ->
    unit
    -> script
end
val key: char -> command
val script: script
val prepend: command -> script -> script
val chain: script -> script -> script
val ( ^> ): command -> script -> script
val execute: script -> unit
