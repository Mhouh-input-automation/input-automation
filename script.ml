open Printf

type command = string
type script = out_channel -> unit

let script o = fprintf o ""
let prepend command script o =
  Printf.kfprintf script o "%s " command
let ( ^> ) = prepend
let chain script script' o =
  script o; script' o
let execute script =
  let out = Unix.open_process_out "xdotool -" in
  (*let out = stdout in*)
  script out;
  close_out out

module Mouse = struct
  let ( @ ) f (x, y) = f x y
  type button = Left | Right
  let button_code = function Left -> 1 | Right -> 3
  let mouse_action action ?(button = Left) () =
    sprintf "%s %d" action (button_code button)
  let click, press, release =
    mouse_action "click",
    mouse_action "mousedown",
    mouse_action "mouseup"
  let move x y = sprintf "mousemove %d %d" x y
  let drag
      ?(button = Left)
      ~initial
      ?(checkpoints = [])
      ~final
      () =
    (move @ initial)
    ^> press ~button ()
    ^> ((fun script -> List.fold_left (fun acc e -> (move @ e) ^> acc) script (List.rev checkpoints))
          ((move @ final) ^> release ~button () ^> script))

end

let key k = sprintf "key %c" k
